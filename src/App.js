import React, { Component } from "react";
import firebase from "firebase/app";
import { throttle } from "lodash";
import "./App.css";

const firebaseConfig = {
  apiKey: "AIzaSyBPLU46jBLMB2SbnorP2AiOT0r9ugEbppo",
  authDomain: "hex-generator.firebaseapp.com",
  databaseURL: "https://hex-generator.firebaseio.com",
  projectId: "hex-generator",
  messagingSenderId: "34530145673",
  appId: "1:34530145673:web:8144e9d7a021f808"
};

firebase.initializeApp(firebaseConfig);

export default class App extends Component {
  state = {
    colors: ["#811b27", "#ed423e", "#00ff6a", "#db2c47"]
  };

  _checkIfNeedsMoreContent = () => {
    // pixels from window bottom to document bottom
    // always equals 0 when scrolling position is in the bottom of the document
    const pixelsToBottom =
      document.documentElement.offsetHeight -
      window.pageYOffset -
      window.innerHeight;

    return pixelsToBottom < 300;
  };

  _generateRandomColor = () =>
    "#" + ((Math.random() * 0xffffff) << 0).toString(16);

  _handleScroll = throttle(() => {
    const { colors } = Object.assign({}, this.state);

    if (this._checkIfNeedsMoreContent()) {
      colors.push(this._generateRandomColor());
    }

    this.setState({
      colors: colors
    });

    console.info("colors.length: ", colors.length);
  }, 300);

  componentDidMount() {
    window.addEventListener("scroll", this._handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this._handleScroll);
  }

  render() {
    const { colors } = this.state;

    return (
      <>
        {colors.map((color, index) => {
          return (
            <div
              key={index}
              className="color"
              style={{ background: color }}
              onWheel={this._handleScroll}
            >
              {color}
            </div>
          );
        })}
      </>
    );
  }
}
